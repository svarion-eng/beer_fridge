# IoT desktop fridge
# Practical IoT with RaspberryPi
# UPM ETSIDI
# 
# Filippo Rigobon 12/2017 - 01/2018

# =================================
#      THIS IS THE MAIN
# =================================

# loading required libraries
import RPi.GPIO as GPIO			# used to control GPIO ports
import time						# to have time-drive functions
import os						# to see system devices
import glob						# for the temperature sensor
import threading				# to create and use threads
from gpiozero import LightSensor	# to use Light sensor
from gpiozero import Motor		# to control motors via PWM

# loading software functions
import auto_control
import sensors

# setting up RasPi in GPIO mode
GPIO.setmode(GPIO.BCM)


# assigning ports (to be revised)
pins = {
		"f_on": 17,		# led: fridge is on
		"heat": 27,		# peltier working, temp not reached
		"t_ok": 22,		# temperature reached
		"d_led": 4,		# door is open
		"pelt": 5,		# Relay ctrl: Peltier power
		"pwm_in": 23,	# PWM signal for internal fan
		"pwm_out": 24,	# PWM signal for external fan
		"temp_in": 14,	# internal temperature sensor
		"temp_out2": 15,# external temperature sensor
		"d_sens": 18,	# reed sensor: door open?
		"m_onoff": 25,	# light sensor to manually turn on or off
		}

# setting up ports
# led
GPIO.setup(pins["f_on"],GPIO.OUT)
GPIO.setup(pins["heat"],GPIO.OUT)
GPIO.setup(pins["t_ok"],GPIO.OUT)
GPIO.setup(pins["d_led"],GPIO.OUT)

# relay
GPIO.setup(pins["pelt"],GPIO.OUT)

# PWM (check how to control it)
GPIO.setup(pins[f_on],GPIO.OUT)
GPIO.setup(pins[f_on],GPIO.OUT)

# temperature sensors

# reed sensor: door presence
GPIO.setup(pins["d_sens"], GPIO.IN, pull_up_down=GPIO.PUD_UP)

# light sensor as on/off (check code)


# creating global variables
global on_off = 0		# fridge state is set to off by default
global emer   = 0		# emergency state is off
global t_temp = 5		# target temperature is 5 °C
global temps  = [20,20]	# actual internal and external temperatures
global door				# door status: 1=open; 0=closed

# DEF. OF MAIN CONTROL THREAD
class ctrl_thr(threading.Thread):
	def __init__(self, threadID, name):
		threading.Thread.__init__(self)
		self.conn = conn
		self.threadID = threadID
		self.name = name
		self.addr = addr

	def run(self, pins):
		print ("Starting main thread")
		auto_control.run(pins)
		print ("Exiting main thread")


# DEF. OF LOW FREQUENCY MONITOR THREAD
#	- updating temp sensors
#	- updating door status
#	- updating soft turn-off
class lowfrew_thr(threading.Thread):
	def __init__(self, threadID, name):
		threading.Thread.__init__(self)
		self.conn = conn
		self.threadID = threadID
		self.name = name
		self.addr = addr

	def run(self,pins):
		global temps
		print ("Starting main thread")
		temps = sensors.temp()			# verificare cosa si debba passare per leggere la temperatura
		sensors.on_off_chk(pins)
		sensors.door_chk(pins)
		print ("Exiting main thread")


# DEF. OF EMERGENCY MONITORING THREAD
class highfrew_thr(threading.Thread):
	def __init__(self, threadID, name):
		threading.Thread.__init__(self)
		self.conn = conn
		self.threadID = threadID
		self.name = name
		self.addr = addr

	def run(self, pins):
		global temps
		print ("Starting main thread")
		temps = sensors.set_emer(pins)
		print ("Exiting main thread")
		
		
		
