import RPi.GPIO as GPIO
from gpiozero import Motor
from gpiozero import PWMOutputDevice
from gpiozero import LightSensor
import time
import os
import glob

#variables
import bf_vars
import pinset

#sensors and actuators functions
import temperature_2x as rtemp
import sensors
import control


#starting system
bf_vars.onoff = True

#show user it is starting
i = 3
while i >= 1:
	GPIO.output(pins["LEDR1"], True)
	time.sleep(0.2)
	GPIO.output(pins["LEDY1"], True)
	time.sleep(0.2)
	GPIO.output(pins["LEDG1"], True)
	time.sleep(0.2)
	GPIO.output(pins["LEDR2"], True)
	time.sleep(0.2)
	GPIO.output(pins["LEDR1"], False)
	time.sleep(0.2)
	GPIO.output(pins["LEDY1"], False)
	time.sleep(0.2)
	GPIO.output(pins["LEDG1"], False)
	time.sleep(0.2)
	GPIO.output(pins["LEDR2"], False)
	time.sleep(0.2)


#actually turning on
control.turnon()



def mainloop()
	while bf_vars.onoff:
		control.mainctrl()
		sensors.luce()
		time.sleep (5)