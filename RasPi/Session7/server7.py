#Servidor que queda a la espera de que se conecte algún cliente, 
#reciba dos sumandos, calcule el resultado y se lo envíe al cliente
#!/usr/bin/python3
import socket
import sys
import threading
 
class myThread (threading.Thread):
   def __init__(self, threadID, name, conn, addr):
      threading.Thread.__init__(self)
      self.conn = conn
      self.threadID = threadID
      self.name = name
      self.addr = addr


   def run(self):
      print ("Starting " + self.name)
      sum2(self.conn, self.addr)#print_time(self.name, self.counter, 5)
      print ("Exiting " + self.name) 
 
def sum2(conn, addr):
	print ("Connection from: " + str(addr))
	data1 = conn.recv(1024).decode()

	print ("Num1: " + str(data1))
	data2 = conn.recv(1024).decode()


	print ("Num2: " + str(data2)) 
	sum = int(data1) + int(data2)
	print ("sending sum: " + str(sum))
	conn.send(str(sum).encode())

def server():
	# Check number of parameters
	if len (sys.argv) != 3:
		print('Usage: python3 server.py IP PORT')
		sys.exit(1)
	host = sys.argv[1]
	port = int(sys.argv[2])
	mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	mySocket.bind((host,port))
	mySocket.listen(2)
	ii = 0
	
	while True:
		conn, addr = mySocket.accept()
		thread1 = myThread(ii, "Thread-1", conn, addr)
		thread1.start()
		thread1.join()
		ii +=1
		
	conn.close()
   

   
server()
