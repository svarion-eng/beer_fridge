
import RPi.GPIO as GPIO
import time
import os
import glob

import bf_vars
import pinset


def switch_relay(val,pin):
    if val:
        val = False
        GPIO.output(pin, True)
    else:
        val = True
        GPIO.output(pin, False)
    return val
	
def doorstate():
    state= GPIO.input(pinset.pins["REEDD"])
    if state == False:
		GPIO.output(pinset.pins["LEDR2"],False)
    else:
		GPIO.output(pinset.pins["LEDR2"],True)
	bf_vars.door = state
	
def luce():
	ldr = LightSensor(pinset.pins["ONOFF"])
	if ldr.value > bf_vars.lhlim:
       ind = 0
    else :
       ind = 1
	   
	if ind:
		if bf_vars.onoff:
			bf_vars.onoff = False
		else:
			bf_vars.onoff = True