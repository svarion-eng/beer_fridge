from gpiozero import Motor
from gpiozero import PWMOutputDevice
import RPi.GPIO as GPIO
import time
import os
import glob


#setting up GPIO Raspberry
GPIO.setmode(GPIO.BCM)
relay=11
GPIO.setup(relay,GPIO.OUT)


def switch_relay(val,pin):
    if val:
        val = False
        GPIO.output(pin, True)
    else:
        val = True
        GPIO.output(pin, False)
    return val


motor = Motor(4,14)
speed = PWMOutputDevice(17, frequency=1000)
speed.value= 0.5
i = 5

while i >= 1:
    speed.value = 0
    val_r = False
    val_r = switch_relay(val_r,relay)
    motor.forward()
    print("stop")
    time.sleep(2)
    
    val_r = True
    speed.value = 1
    val_r = switch_relay(val_r,relay)
    motor.forward()
    print("going topspeed")
    time.sleep(2)
    i -= 1

speed.value = 0.01
val_r = False
val_r = switch_relay(val_r,relay)
motor.forward()
print("10½ and stopping 3 pin")
time.sleep(60)
motor.stop()
print("stopping PWM")
