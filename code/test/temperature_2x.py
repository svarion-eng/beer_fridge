#!/usr/bin/python3
# Instrucciones de configuracion de la sonda
# sudo modprobe w1-gpio
# sudo modprobe w1-therm
# cd /sys/bus/w1/devices/
# ls
# cd 28-XXX
# cat w1_slave

import os
import glob
import time
 
os.system('modprobe w1-gpio')
os.system('modprobe w1-therm')
 
base_dir = '/sys/bus/w1/devices/'
device_folder1 = glob.glob(base_dir + '28*')[0]
device_folder2 = glob.glob(base_dir + '28*')[1]
temp_sens1 = device_folder1 + '/w1_slave'
temp_sens2 = device_folder2 + '/w1_slave'
 
def read_temp_raw(ind):
    if ind==1:
        f = open(temp_sens1 , 'r')
        lines= f.readlines()
        f.close()
    else:
        f = open(temp_sens2 , 'r')
        lines = f.readlines()
        f.close()
    return lines
 
def read_temp(ind):
    lines = read_temp_raw(ind)
    while lines[0].strip()[-3:] != 'YES':
        time.sleep(0.2)
        lines = read_temp_raw(ind)
    equals_pos = lines[1].find('t=')
    if equals_pos != -1:
        temp_string = lines[1][equals_pos+2:]
        temp_c = float(temp_string) / 1000.0
        temp_f = temp_c * 9.0 / 5.0 + 32.0
        return temp_c

# TEST
print(read_temp(1))
print(read_temp(2))
