import RPi.GPIO as GPIO
from gpiozero import LightSensor
import time
import os
import glob


pins = {
    "LEDR1": 6,		# R1 led: fridge is on
    "LEDY1": 19,	# Y1 led: peltier working, temp not reached
    "LEDG1": 13,	# G1 led: temperature reached
    "LEDR2": 26,	# R2 led: door is open
    "RELPL": 9,		# Relay ctrl: Peltier power
    "RELIF": 11,	# Relay ctrl: internal fan
    "PWMEF": 17,	# PWM signal for external fan
    "TEMP":  4,	        # 1 wire bus: temperature sensors
    "REEDD": 5,         # reed sensor: door open?
    "ONOFF": 10,	# light sensor to manually turn on or off
    }



GPIO.setmode(GPIO.BCM)

GPIO.setup(pins["LEDR1"],GPIO.OUT)
GPIO.setup(pins["LEDY1"],GPIO.OUT)
GPIO.setup(pins["LEDG1"],GPIO.OUT)
GPIO.setup(pins["LEDR2"],GPIO.OUT)
GPIO.setup(pins["REEDD"], GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(pins["RELPL"],GPIO.OUT)
GPIO.setup(pins["RELIF"],GPIO.OUT)
GPIO.setup(pins["PWMEF"],GPIO.OUT)


print("leds and reed setup: OK")



#Temp
os.system('modprobe w1-gpio')
os.system('modprobe w1-therm')
 
base_dir = '/sys/bus/w1/devices/'
device_folder1 = glob.glob(base_dir + '28*')[0]
device_folder2 = glob.glob(base_dir + '28*')[1]
temp_sens1 = device_folder1 + '/w1_slave'
temp_sens2 = device_folder2 + '/w1_slave'

print("temperature sensors setup: OK")


#Light Sensor

ldr = LightSensor(pins["ONOFF"])
ldr.threshold = 0.77

print("Lightsensor setup: OK ", str(ldr.threshold))




# test section
GPIO.output(pins["LEDR1"], True)
GPIO.output(pins["LEDY1"], True)
GPIO.output(pins["LEDG1"], True)
GPIO.output(pins["LEDR2"], True)

time.sleep(2)

GPIO.output(pins["LEDR1"], False)
GPIO.output(pins["LEDY1"], False)
GPIO.output(pins["LEDG1"], False)
GPIO.output(pins["LEDR2"], False)


def read_temp_raw(ind):
    if ind==1:
        f = open(temp_sens1 , 'r')
        lines= f.readlines()
        f.close()
    else:
        f = open(temp_sens2 , 'r')
        lines = f.readlines()
        f.close()
    return lines
 
def read_temp(ind):
    lines = read_temp_raw(ind)
    while lines[0].strip()[-3:] != 'YES':
        time.sleep(0.2)
        lines = read_temp_raw(ind)
    equals_pos = lines[1].find('t=')
    if equals_pos != -1:
        temp_string = lines[1][equals_pos+2:]
        temp_c = float(temp_string) / 1000.0
        temp_f = temp_c * 9.0 / 5.0 + 32.0
        return temp_c

print(read_temp(1))
print(read_temp(2))

def getStateReed(pin):
    state= GPIO.input(pin)
    if state == False:
        print('Reed Detected')
    else:
        print('Reed Not Detected')

i=10

while i>1:
    getStateReed(pins["REEDD"])
    time.sleep(0.5)
    print (ldr.value)
    if ldr.value > ldr.threshold :
       print("light")
    else :
        print("dark")
    time.sleep(0.5)
    i -= 1


