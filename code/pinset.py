import RPi.GPIO as GPIO
from gpiozero import LightSensor
import time
import os
import glob
import bf_vars


pins = {
    "LEDR1": 6,		# R1 led: fridge is on
    "LEDY1": 19,	# Y1 led: peltier working, temp not reached
    "LEDG1": 13,	# G1 led: temperature reached
    "LEDR2": 26,	# R2 led: door is open
    "RELPL": 9,		# Relay ctrl: Peltier power
    "RELIF": 11,	# Relay ctrl: internal fan
    "PWMEF": 17,	# PWM signal for external fan
    "TEMP":  4,	    # 1 wire bus: temperature sensors
    "REEDD": 5,     # reed sensor: door open?
    "ONOFF": 10,	# light sensor to manually turn on or off
    }



GPIO.setmode(GPIO.BCM)

GPIO.setup(pins["LEDR1"],GPIO.OUT)
GPIO.setup(pins["LEDY1"],GPIO.OUT)
GPIO.setup(pins["LEDG1"],GPIO.OUT)
GPIO.setup(pins["LEDR2"],GPIO.OUT)
GPIO.setup(pins["REEDD"], GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(pins["RELPL"],GPIO.OUT)
GPIO.setup(pins["RELIF"],GPIO.OUT)
GPIO.setup(pins["PWMEF"],GPIO.OUT)


print("leds and reed setup: OK")
print("relay setup: OK")



#Temp
os.system('modprobe w1-gpio')
os.system('modprobe w1-therm')
 
base_dir = '/sys/bus/w1/devices/'
device_folder1 = glob.glob(base_dir + '28*')[0]
device_folder2 = glob.glob(base_dir + '28*')[1]
temp_sens1 = device_folder1 + '/w1_slave'
temp_sens2 = device_folder2 + '/w1_slave'

print("temperature sensors setup: OK")


#Light Sensor

ldr = LightSensor(pins["ONOFF"])
ldr.threshold = bf_vars.lhlim

print("Lightsensor setup: OK ", str(ldr.threshold))


