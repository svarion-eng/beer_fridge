
import RPi.GPIO as GPIO
from gpiozero import Motor
from gpiozero import PWMOutputDevice
from gpiozero import LightSensor
import time
import os
import glob

import temperature_2x as rtemp


#variables
import bf_vars
import pinset
import sensors

motor = Motor(4,14)
speed = PWMOutputDevice(pinset.pins["PWMEF"], frequency=1000)
speed.value= 0.5


#functions
def mainctrl():
	bf_vars.itemp = rtemp.read_temp(1)
	bf_vars.etemp = rtemp.read_temp(2)
	sensors.doorstate()
	if bf_vars.itemp < bf_vars.ttemp:
		GPIO.output(pinset.pins["LEDY1"], False)
		GPIO.output(pinset.pins["RELIF"], False)
		GPIO.output(pinset.pins["RELPL"], False)
		GPIO.output(pinset.pins["LEDG1"], True)
		speed.value = 0
	else:
		GPIO.output(pinset.pins["LEDY1"], True)
		GPIO.output(pinset.pins["RELIF"], True)
		GPIO.output(pinset.pins["RELPL"], True)
		GPIO.output(pinset.pins["LEDG1"], False)
		speed.value = 1


def turnon():
	if bf_vars.onoff:
		GPIO.output(pinset.pins["LEDR1"], True)
		GPIO.output(pinset.pins["LEDY1"], True)
		GPIO.output(pinset.pins["RELIF"], True)
		GPIO.output(pinset.pins["RELPL"], True)
		speed.value = 1
	else:
		GPIO.output(pinset.pins["LEDY1"], False)
		GPIO.output(pinset.pins["RELIF"], False)
		GPIO.output(pinset.pins["RELPL"], False)
		GPIO.output(pinset.pins["LEDG1"], False)
		GPIO.output(pinset.pins["LEDR1"], False)
		GPIO.output(pinset.pins["LEDR2"], False)
		speed.value = 0