import pinset
import RPi.GPIO as GPIO
from gpiozero import Motor
from gpiozero import PWMOutputDevice
from gpiozero import LightSensor
import time
import os
import glob
import temperature_2x as rtemp

def switch_relay(val,pin):
    if val:
        val = False
        GPIO.output(pin, True)
    else:
        val = True
        GPIO.output(pin, False)
    return val




motor = Motor(4,14)
speed = PWMOutputDevice(pinset.pins["PWMEF"], frequency=1000)
speed.value= 1

val_r = True
switch_relay(val_r,pinset.pins["RELIF"])
motor.forward()
time.sleep(5)

switch_relay(True,pinset.pins["RELPL"])

i = 100

while i >= 1:
    temp1 = rtemp.read_temp(1)
    temp2 = rtemp.read_temp(2)
    print(str(temp1))
    print(str(temp2))
    time.sleep(4)
    i -= 1
    print(str(i*4))

switch_relay(False,pinset.pins["RELPL"])
switch_relay(False,pinset.pins["RELIF"])

    
