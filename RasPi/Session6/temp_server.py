#Servidor que queda a la espera de que se conecte algún cliente, 
#reciba dos sumandos, calcule el resultado y se lo envíe al cliente
#!/usr/bin/python3
import socket
import sys
import time
import temperature

def server():
	# Check number of parameters
	if len (sys.argv) != 3:
		print('Usage: python3 server.py IP PORT')
		sys.exit(1)
	host = sys.argv[1]
	port = int(sys.argv[2])
	mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	mySocket.bind((host,port))
	mySocket.listen(1)

	while True:
		conn, addr = mySocket.accept()
		print ("Connection from: " + str(addr))
		data1 = str(conn.recv(1024).decode())
		print (data1)
                
		if data1 == "t":
			#break

                        #print(str(66))
                        result = temperature.read_temp()
                        print ("temperature is: " + str(result))
		
                        #print ("sending data: " + str(result))
                        conn.send(str(result).encode())
	
1     
server()
