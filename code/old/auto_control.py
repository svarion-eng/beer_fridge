# IoT desktop fridge
# Practical IoT with RaspberryPi
# UPM ETSIDI
# 
# Filippo Rigobon 12/2017 - 01/2018

# =================================
#      CONTROL LOOP
# =================================

# loading globals
global emer
global on_off
global t_temp
global temps

def emer_off(pins):
	global emer
	if emer:
		GPIO.setup(pins["f_on"],True)
		GPIO.setup(pins["heat"],True)
		GPIO.setup(pins["t_ok"],True)
		GPIO.setup(pins["d_led"],True)
		# turn off motors
		GPIO.setup(pins["pelt"],False)
		
def go_off(pins):
	# what happens when user sets on_off to 0


def speed_ctrl(pins):
	global temps
	# speed control of fan 1
	# speed control of fan 2
	
def pelt_ctrl(pins):
	global temps
	global t_temp
	if temps[0]<t_temp:
		GPIO.setup(pins["heat"],True)
		GPIO.setup(pins["pelt"],True)
		GPIO.setup(pins["t_ok"],False)
	else:
		GPIO.setup(pins["heat"],False)
		GPIO.setup(pins["pelt"],False)
		GPIO.setup(pins["t_ok"],True)
		
	
def run(pins):
	global on_off
	global t_temp
	global temps
	
	while on_off
		if emer:
			emer_off(pins)
			continue
		speed_ctrl(pins)
		pelt_ctrl(pins) 
	
	go_off(pins)
	