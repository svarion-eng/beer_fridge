#here are contained all the global variables

onoff = True	#Fridge should be active
emerg = False	#emergency stop: if true, all goes off (except HOTside fan) and LEDs turn all on
itemp = 1		#internal temperature
htemp = 1		#temperature of hotside heatsink
ttemp = 15		#target temperature
door  = True	#door state: 1 is open, 0 is closed
fanin = False	#state of internal fan
fanex = 0		#PWM value of external fan
pelpw = False	#state of Peltier cell
lhlim = 0.77	#light sensor threshold
