import RPi.GPIO as GPIO
import time
import os
import glob


#setting up GPIO Raspberry
GPIO.setmode(GPIO.BCM)

Pin_led1=6
Pin_led2=19
Pin_led3=13
relay1=9
relay2=11

GPIO.setup(Pin_led1,GPIO.OUT)
GPIO.setup(Pin_led2,GPIO.OUT)
GPIO.setup(Pin_led3,GPIO.OUT)
GPIO.setup(relay1,GPIO.OUT)
GPIO.setup(relay2,GPIO.OUT)


def Func_Set_LED(val,pin):
    if val:
        val = False
        GPIO.output(pin, True)
    else:
        val = True
        GPIO.output(pin, False)
    return val

def switch_relay(val,pin):
    if val:
        val = False
        GPIO.output(pin, True)
    else:
        val = True
        GPIO.output(pin, False)
    return val

j=30
val1 = True
val2 = True
val3 = True
val_r1 = False
val_r2 = False

time.sleep(5)

while j:
    val1 = Func_Set_LED(val1,Pin_led1)
    time.sleep(0.1)
    val2 = Func_Set_LED(val2,Pin_led2)
    time.sleep(0.1)
    val_r1 = switch_relay(val_r1,relay1)
    print(str(val_r1))
    time.sleep(0.3)
    val_r2 = switch_relay(val_r2,relay2)
    print(str(val_r2))
    time.sleep(0.3)
    val3 = Func_Set_LED(val3,Pin_led3)
    time.sleep(1)
    j -= 1
    print(str(j))
    


